
# Lead

A simple clipboard capturing app that you can trust on

ClipboardMemo is capturing, storing and managing clipboard content. It's a simple and free dock application.


# Why choose ClipboardMemo ?

ClipboardMemo is more than just a clipboard capturing app.

### Capture and store

All new contents of the clipboard are automatically stored

### Paste any stored clip

Each entry can be placed back in the clipboard

### Manage stored clips

Clean and compact the stored entries


# Features and details

### Hold capture

The capture may be temporarily suspended (for example, to copy confidential content)

### Compact all entries into one

All entries in the history can be grouped into one.

### Automatically saved

All entries and preferences are automatically saved for each user.

### Multi-languages

The following languages are supported :
- English
- French
- Spanish

### Opensource and free

ClipboardMemo is a modern, opensource and free application. It is coded with NodeJS and Electron.

### Limitations

- the history is limited to the last 80 contents of the clipboard
- only the last 16 entries are displayed
- this application is only available on MacOS
