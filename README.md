# ClipboardMemo README

Copyright 2018 - Fabrice Romand <fabrice.romand@gmail.com>  
ISC License (see LICENSE file)  
Icons credit :
- Sergey Ershov - https://www.iconfinder.com/iconsets/multimedia-75
- Bogdan Rosu - http://bogdanrosu.com/

App website : https://clipboardmemo.romand.ovh  
Project Status: Master ![Build Status](https://gitlab.com/fabrom/clipboardmemo/badges/master/build.svg) - Dev ![Build Status](https://gitlab.com/fabrom/clipboardmemo/badges/dev/build.svg)

## Description

This application capture, store and help managing all clipboard new contents (raw/rtf/html texts and PNG/JPEG images).
It's a simple dock application for Linux, MacOS and Windows.

Each record can be
- replayed by a simple click on it
- moved to clipboard (Cmd+Alt+V). Extracted records are remove from history
- removed

Even more, the historic can be :
- totally cleaned
- packed into one single entry (text only)

Clipboard capture can be paused.

Currently, the following languages are supported :
- English
- French
- Spanish

## Limits 
Currently limits applied :
- max. 80 records (rotating list)
- max. 16 last records are displayed

## Installing the dependencies

  \> yarn install

## Running application from the sources

  \> yarn run start

  or
  
   \> yarn run dev (for development mode)

## Building packages

Build for all platforms (Linux, MacOS,  Windows)

  \> yarn dist

Build only for one platform

  \> yarn dist:linux  
  \> yarn dist:darwin  
  \> yarn dist:win32

DMG packages will be in *./dist/* directory

## Issues reporting

https://gitlab.com/fabrom/clipboardmemo/issues

## Contributions

https://gitlab.com/fabrom/clipboardmemo/merge_requests
